import {
  Arc,
  DAO,
  Proposal,
  Stake,
  Vote,
  Address,
  Member,
  Reward
} from "@daostack/client";
import dotenv from "dotenv";
import ethers from "ethers";
import fs from "fs";
import BN from "bn.js";
dotenv.config();

const arcRinkeby = new Arc({
  graphqlHttpProvider:
    "https://rinkeby.subgraph.daostack.io/subgraphs/name/v24",
  graphqlWsProvider: "wss://ws.rinkeby.subgraph.daostack.io/subgraphs/name/v24",
  web3Provider: `wss://rinkeby.infura.io/v3/${process.env.INFURA}`
});

/*
const arcMain = new Arc({
  graphqlHttpProvider: "https://subgraph.daostack.io/subgraphs/name/v24",
  graphqlWsProvider: "wss://ws.subgraph.daostack.io/subgraphs/name/v24",
  web3Provider: `wss://mainnet.infura.io/v3/${process.env.INFURA}`
});
*/

///0x3cdb39c1ca57c19a316dd2f5acf900705220c713
// get a list of DAOs
// stage of a proposal is whether it is still open or not

const GustatoryEngineering = "0x3cdb39c1ca57c19a316dd2f5acf900705220c713";

/* queries that work
  const contracts = await arc.fetchContractInfos();
  const dao = await DAO.search(arc, { where: { name: "dxDAO" } }).forEach(

    
  const dao = await DAO.search(arcRinkeby, {
    where: { name: "dxDAO" }
  }).forEach(next => {
    next[0].state().forEach(next => {
      console.log("nest ", next);
    });
  });
      */
/*
  (property) IProposalQueryOptions.where?: {
    [key: string]: any;
    accountsWithUnclaimedRewards_contains?: string[] | undef    .proposals({ where: { dao: "0x3cdb39c1ca57c19a316dd2f5acf900705220c713" } })ined;
    active?: boolean | undefined;
    boosted?: boolean | undefined;
    dao?: string | undefined;
    expiresInQueueAt?: number | undefined;
    ... 12 more ...;
    type?: IProposalType | ... 2 more ... | undefined;
} | undefined
*/

function getProposalInfo() {
  arcRinkeby
    .dao(GustatoryEngineering)
    .proposals()
    .forEach(proposal => {
      proposal.forEach(prop => {
        prop.state().forEach(state => {
          if (
            state.id ==
            "0x8786fe5cec51a1b13faf2c255e2d7e2ecaace14fa85831dd4b932c0f046b0379"
          ) {
            console.log("SUP");
            var {
              proposal: { id },
              confidenceThreshold,
              title,
              stakesAgainst,
              stakesFor,
              totalRepWhenCreated,
              totalRepWhenExecuted,
              contributionReward
            } = state;
            stakesAgainst = new BN(stakesAgainst);
            stakesFor = new BN(stakesFor);
            /*
            totalRepWhenCreated = new BN(totalRepWhenCreated);
            totalRepWhenExecuted = new BN(totalRepWhenExecuted);
            */

            console.log(
              "hi ",
              confidenceThreshold,
              title,
              stakesAgainst.toString(),
              stakesFor.toString()
            );
          }
        });
      });
    });
}

async function getDaoInfo() {
  Proposal.search(arcRinkeby, { where: { title: "portis rep" } }).forEach(
    proposal => {
      proposal.forEach(proposal => {
        proposal.state().forEach(state => {
          console.log(state);
        });
      });
    }
  );
}
async function main() {
  //getProposalInfo();
  getDaoInfo();
}

main();
