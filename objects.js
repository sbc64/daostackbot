{ accountsWithUnclaimedRewards:
   [ '0x0084fb1d84f2359cafd00f92b901c121521d6809',
     '0x2c932fb57c710bf1e5a54de904dae3d82e078f71' ],
  boostedAt: 1565134312,
  confidenceThreshold: 2199023255552,
  contributionReward:
   { alreadyRedeemedEthPeriods: 0,
     alreadyRedeemedExternalTokenPeriods: 0,
     alreadyRedeemedNativeTokenPeriods: 0,
     alreadyRedeemedReputationPeriods: 1,
     beneficiary: '0x2c932fb57c710bf1e5a54de904dae3d82e078f71',
     ethReward: <BN: 0>,
     externalToken: '0x543ff227f64aa17ea132bf9886cab5db55dcaddf',
     externalTokenReward: <BN: 0>,
     nativeTokenReward: <BN: 0>,
     periodLength: 0,
     periods: 1,
     reputationReward: <BN: 12f939c99edab80000> },
  createdAt: 1565133202,
  dao:
   DAO {
     context:
   queuedVotePeriodLimit: 1800,
     queuedVoteRequiredPercentage: 50,
     quietEndingPeriod: 300,
     thresholdConst: 2,
     votersReputationLossRatio: 1 },
  id:
   '0xf267611df08f0067cb8a5c367539c05655e42b1d1ff2d14c82df5bb9980438e6',
  organizationId:
   '0x9bf723962df9d70b22eabae173a559cc0fd665bf0f6b1857e15ab3a657ab3d71',
  paramsHash: undefined,
  preBoostedAt: 1565133547,
  proposal:
   Proposal {
     id:
      '0xf267611df08f0067cb8a5c367539c05655e42b1d1ff2d14c82df5bb9980438e6',
     context:
  proposer: '0x0084fb1d84f2359cafd00f92b901c121521d6809',
  queue:
   { dao: '0x3cdb39c1ca57c19a316dd2f5acf900705220c713',
     id:
      '0x9bf723962df9d70b22eabae173a559cc0fd665bf0f6b1857e15ab3a657ab3d71',
     name: 'ContributionReward',
     scheme:
      { address: '0x88fba19bf273cf75945ded8986745da140a99145',
        canDelegateCall: false,
        canManageGlobalConstraints: false,
        canRegisterSchemes: false,
        canUpgradeController: false,
        dao: '0x3cdb39c1ca57c19a316dd2f5acf900705220c713',
        id:
         '0xfc796ca45ec8a9fde48a3cf9abe0d3e54235e628c9d67ba4bfbeafbad427f8af',
        name: 'ContributionReward',
        paramsHash:
         '0x75aaec42f90dd56d86dee37e989e0612152f2dc341baf8feaefac983b8a18f62' },
     threshold: 1,
     votingMachine: '0xe3692ad4ed2d2817bea59aed435ce17d28e884eb' },
  quietEndingPeriodBeganAt: 0,
  resolvedAt: 0,
  scheme:
   { address: '0x88fba19bf273cf75945ded8986745da140a99145',
     canDelegateCall: false,
     canManageGlobalConstraints: false,
     canRegisterSchemes: false,
     canUpgradeController: false,
     dao: '0x3cdb39c1ca57c19a316dd2f5acf900705220c713',
     id:
      '0xfc796ca45ec8a9fde48a3cf9abe0d3e54235e628c9d67ba4bfbeafbad427f8af',
     name: 'ContributionReward',
     paramsHash:
      '0x75aaec42f90dd56d86dee37e989e0612152f2dc341baf8feaefac983b8a18f62' },
  schemeRegistrar: null,
  stage: 1,
  stakesAgainst: <BN: 56bc75e2d63100000>,
  stakesFor: <BN: ae56f730e6d840000>,
  title: 'give rep to portis',
  totalRepWhenCreated: <BN: 1fda55f153aeadc8000>,
  totalRepWhenExecuted: <BN: 1fda55f153aeadc8000>,
  type: 'ContributionReward',
  upstakeNeededToPreBoost: <BN: 0>,
  url: '',
  voteOnBehalf: undefined,
  votesAgainst: <BN: 0>,
  votesCount: 1,
  votesFor: <BN: 3522fe652ee11d8000>,
  votingMachine: '0xe3692ad4ed2d2817bea59aed435ce17d28e884eb',
  winningOutcome: 1 }